package com.jz.baiduHotSearch;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

public class Client {

    @Test
    void parseXml() throws IOException {
        String filePath = "C:\\Users\\jizho\\Documents\\WXWork\\1688855482413706\\Cache\\File\\2023-12\\20231224户外骑行.gpx";
        Document document = Jsoup.parse(new File(filePath), "utf-8");
        Elements elements = document.getElementsByTag("trkpt");
        for (Element item : elements) {
            Attributes attributes = item.attributes();
            StringBuilder res = new StringBuilder();
            for (Attribute attribute : attributes) {
                String key = attribute.getKey();
                String value = attribute.getValue();
                res.append(value).append("\t");
            }
            Elements childrenElements = item.children();
            for (Element childItem : childrenElements) {
                String text = childItem.text();
                res.append(text).append("\t");
            }
            System.out.println(res.toString());
        }
    }
}
