package com.jz.baiduHotSearch.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jz.baiduHotSearch.utils.WorkCommonUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 水利知识文件抓取
 *
 * @author jilt
 * @Date 2024/9/13 15:34
 */
public class ShuiLiDocumentTest {
    private static String OUT_FILE_PATH = "C:/wavenet/work-demo/"+ DateUtil.today();

    @Test
    void shulibuTest() throws IOException {
        String url = "http://www.mwr.gov.cn/szs/slgc/zmfhgc/";
        String cookie = "enable_zPg94gdHIAJH=true; wdcid=5bc4402371d791ba; zPg94gdHIAJHS=58wrrF8iQaQOdb.OyelrGOvoBendKhEiTxxc37IIFgyTvYAAbxdwcDuXJSdcWHEmNaBoru4Vssn1P.x918emUuA; __FT10000001=2024-9-13-14-48-29; __NRU10000001=1726210109838; __FT10000054=2024-9-13-16-23-2; __NRU10000054=1726215782085; zhuzhan=91349450; __REC10000001=2; __RT10000001=2024-9-18-13-47-58; __REC10000054=1; __RT10000054=2024-9-18-14-42-56; 8b72903967b2f15179=e42a3f62612fc3e3d64055d35f068a4e; __FT10000003=2024-9-18-15-55-42; __NRU10000003=1726646142275; __RT10000003=2024-9-18-15-55-42; zPg94gdHIAJHT=5RXjY7KMj2MqqqqD1rmPeSacu8oJYT5OBP.jxH4TyLCAVETlqU7dJcXHai_slh1KbZ3X9p5.mce6BLNST7Ub62FUmpUyvbOGXG58wNDLE.5wxQOQG6JsU6DrGQNtThW9fKOUH4k3YTDCV0.0oOKdlXy64NgmW5EfXSISXZDAeSoEh34ZpyJ1EFdO_MUSbn8YiGb_pVHNc7lE3ND_cI8PW65tbJotGdFfln_jcst9wLev2WIoGIwv.3iR7tvsC03v9aFe75hVWYMoof9tc3RIiKd";
        Document document =  Jsoup.connect(url).header("cookie",cookie).get();
        Element dev = document.select("body > div.main > div.mainc > div.newscontain > div").get(0);
        Elements elements = dev.getElementsByTag("a");
        System.out.println(elements.size());
        for (Element item : elements) {
            Attribute href1 = item.attribute("href");
            String fileName = item.text();
            String uri = href1.getValue().replaceAll("\\./","");
            String fileUrl = url + uri;
            System.out.println(fileName +":"+fileUrl);
            shuilibuContent(fileName,fileUrl,cookie);
        }
    }

    @Test
    void shuilibuContent(String fileName,String url,String cookie){
        fileName = fileName + ".txt";
        try {
//            String cookie = "wdcid=5bc4402371d791ba; zPg94gdHIAJHS=58wrrF8iQaQOdb.OyelrGOvoBendKhEiTxxc37IIFgyTvYAAbxdwcDuXJSdcWHEmNaBoru4Vssn1P.x918emUuA; __FT10000001=2024-9-13-14-48-29; __NRU10000001=1726210109838; __FT10000054=2024-9-13-16-23-2; __NRU10000054=1726215782085; zhuzhan=91349450; __REC10000001=2; __RT10000001=2024-9-18-13-47-58; __REC10000054=1; __RT10000054=2024-9-18-14-42-56; 8b72903967b2f15179=e42a3f62612fc3e3d64055d35f068a4e; __FT10000003=2024-9-18-15-55-42; __NRU10000003=1726646142275; __RT10000003=2024-9-18-15-55-42; zPg94gdHIAJHT=5RXjtQDMjPvqqqqD1rcZKTGCB2KQuEx60rHeUYJTmaOVNL1LJpH9gCoO3_MeZu8xiXF7YS71ShgRXuWm14aJeexhJ8qu5Gor7nYw_1R0bYMXwglG1OOaQyRe0dj._p7F5caM6.Do82CmtYCIOYt2vpKlMAYMN5HlWGS81LMIXHkDtZyqMSkku_Pc5erytl5DQBcFCCnY1tPS5ZQq.WsWiphuz7sWwO9JuAzjrHnf_AiXJrNO_d1BdmFTDQVTlee9u9eiXIxWFmACfrXFXRlo7.fs3aufKweP.IkRUSiZNA.v1xx6rk9O8U9xyBvgma8TXY4sL3FU8DNvMTdvQNdLRHW";
            Document document =  Jsoup.connect(url).header("cookie",cookie).get();
//            Elements select = document.select(".gknb_content");
            Element div = document.select("div.TRS_Editor").get(0);
//            String text = div.text();
//            System.out.println(text);
            Elements pList = div.getElementsByTag("p");
            StringBuilder sb = new StringBuilder();
            for (Element element : pList) {
                sb.append(element.text()).append("\n");
            }
//            System.out.println(sb.toString());
            WorkCommonUtils.createFile(OUT_FILE_PATH,fileName,sb.toString());
//            WorkCommonUtils.createFile(OUT_FILE_PATH,fileName,text);
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println(fileName);
    }

    /**
     * 浦东新区生态环境局（水务局）
     * @throws IOException
     */
    @Test
    void pdZhuaQuMuluPage() throws IOException {
        String url = "https://www.pudong.gov.cn/zwgk-search-front/api/data/affair";
        pdZhuaQuMulu(url);
    }

    void pdZhuaQuMulu(String url) throws IOException {
        Map<String,Object> params = new HashMap<>();
        String[] arrStr = {"14913"};
        params.put("channelList",arrStr);
        params.put("pageNo",1);
        params.put("pageSize",10000);
        String reqRes = HttpRequest.post(url).body(JSON.toJSONString(params)).timeout(3000).execute().body();
//        System.out.println(reqRes);
        JSONObject resJsonObj = JSONObject.parseObject(reqRes);
        JSONObject data = (JSONObject)resJsonObj.get("data");
        JSONArray list = (JSONArray)data.get("list");
        int n = 0;
        for (Object item : list) {
            System.out.println(n++);
            JSONObject itemJson = (JSONObject)item;
            String title = (String)itemJson.get("title");
            String fileUrl = (String)itemJson.get("url");
            System.out.println(title+": "+fileUrl);
            pdContent(title,fileUrl);
        }
    }

    @Test
    void pdContent(String fileName,String url) {
        fileName = fileName + ".txt";
        try {
            Document document =  Jsoup.connect(url).get();
            Element div = document.select("#ivs_content").get(0);
            Elements pList = div.getElementsByTag("p");
            StringBuilder sb = new StringBuilder();
            for (Element element : pList) {
                sb.append(element.text()).append("\n");
            }
            WorkCommonUtils.createFile(OUT_FILE_PATH,fileName,sb.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 上海市水务局
     * @throws IOException
     */
    @Test
    void shZhuaQuMuluPage() throws IOException {
        String url = "https://swj.sh.gov.cn/swyw/index.html";
        for (int i = 0; i < 592; i++) {
            String reqUrl = url;
            if(i > 0){
                int n = i +1;
                String index = "index_" + n;
                reqUrl = url.replaceAll("index",index);
            }
            zhuaQuMulu(reqUrl);
        }
    }

    @Test
    void zhuaQuMulu(String url) throws IOException {
//        String url = "https://swj.sh.gov.cn/gfxwj2022/index.html";

        Document document =  Jsoup.connect(url).get();
        Element dev = document.select("#gzfw_contList > ul").get(0);
        Elements elements = dev.getElementsByTag("a");
        System.out.println(elements.size());
        for (Element item : elements) {
            Attribute href1 = item.attribute("href");
            String fileName = item.text();
            String uri = href1.getValue();
            if(uri.contains("http")){
                continue;
            }
            String fileUrl = "https://swj.sh.gov.cn" + uri;
            System.out.println(fileUrl);
            zhuaQuContent(fileName,fileUrl);
        }
    }

    /**
     * 抓取文本内容
     * @throws IOException
     */
//    @Test
    void zhuaQuContent(String fileName,String url) throws IOException {
//        String fileName = "上海市水务局关于印发《上海市水土保持管理办法》的通知.txt";
//        String url = "https://swj.sh.gov.cn/gfxwj2022/20240819/eb3a9958467e43f990264e9692495441.html";
        fileName = fileName + ".txt";
        String html= HttpUtil.get(url);
        Document document = Jsoup.parse(html);
        Element div = document.select("#ivs_content").get(0);
        Elements pList = div.getElementsByTag("p");
        StringBuilder sb = new StringBuilder();
        for (Element element : pList) {
            sb.append(element.text()).append("\n");
        }
        WorkCommonUtils.createFile(OUT_FILE_PATH,fileName,sb.toString());
    }
}
