package com.jz.baiduHotSearch.controller;

import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * jsoup数据抓取
 */
class JsoupControllerTest {


    /**
     * 百度热搜数据抓取
     * @throws IOException
     */
    @Test
    void jsoupBaiduTest() throws IOException {
        String url = "https://top.baidu.com/board?tab=realtime";
        Document document = Jsoup.connect(url).get();
        Elements select = document.select(".c-single-text-ellipsis");
        List<TextNode> textNodes = select.textNodes();
        for (TextNode item : textNodes) {
            System.out.println(item);
        }
    }

    /**
     * 中央气象台雷达图片抓取
     *
     * @throws IOException
     */
    @Test
    void jsoupWeatherRadarTest() throws IOException {
        String url = "http://www.nmc.cn/publish/radar/chinaall.html";
        Document document = Jsoup.connect(url).get();
        Elements select = document.select(".col-xs-12").select(".time");
        List<String> imgUrlList = select.eachAttr("data-img");
        List<String> timeList = select.eachAttr("data-time");
        for (int i = 0; i < imgUrlList.size(); i++) {
            System.out.println(timeList.get(i)+" >>> "+imgUrlList.get(i));
        }
    }



    /**
     * 新浪微博数据抓取(参数准备阶段)
     *
     * @throws IOException
     */
    @Test
    void jsoupWbCookieTest() throws IOException {
        String url = "https://passport.weibo.com/visitor/visitor";
        Map<String,Object> params = new HashMap<>();
        params.put("a","incarnate");
        params.put("t",jsoupWbTid());
        params.put("w","3");
        params.put("c","100");
        params.put("cb","cross_domain");
        params.put("from","weibo");
        String reqRes = HttpUtil.get(url, params, 3000);
        System.out.println(reqRes);
        String jsonStr = reqRes.substring(reqRes.indexOf("(") + 1, reqRes.indexOf(")"));
        JSONObject res = JSONObject.parseObject(jsonStr);
        if(res.getIntValue("retcode")==20000000){
            String sub = res.getJSONObject("data").getString("sub");
            String subp = res.getJSONObject("data").getString("subp");
            System.out.println(sub);
            System.out.println(subp);
        }
    }

    /**
     * 新浪微博数据抓取（执行阶段）
     *
     * @throws IOException
     */
    @Test
    void jsoupWbHotTest() throws IOException {
        String url = "https://s.weibo.com/top/summary?cate=realtimehot";
        String cookie = "SUB="+"_2AkMS31Hef8NxqwFRmfoWyGLjaYl0yAnEieKkg6AFJRMxHRl-yT9kqmoetRB6OV9_MU4pb1XZaTreoICatRorx2k6O6DF"+"; SUBP="+"0033WrSXqPxfM72-Ws9jqgMF55529P9D9WFG.5MMI2zWR6IO9iQdcHoI"+";";
        String responseHtml = HttpRequest.get(url).header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36")
                .header("Cookie", cookie).timeout(3000).execute().body();
        Document document = Jsoup.parse(responseHtml);
        Element tbody = document.getElementsByTag("tbody").get(0);
        Elements tr = tbody.getElementsByTag("tr");
        for (Element item : tr) {
            Element indexElement = item.select(".td-01").get(0);
            //排序
            String index = indexElement.text();
            if(!index.matches("\\d+")){
                continue;
            }
            //热搜关键词
            Element contentElement = item.select(".td-02").select("a").get(0);
            String text = contentElement.text();
            //热度指数
            Elements hotElements = item.select(".td-02").select("span");
            String hot = "";
            String keyword = "";
            if(!CollectionUtils.isEmpty(hotElements)){
                String hotRes = hotElements.get(0).text();
                hot = ReUtil.get("\\d+",hotRes,0);
                keyword = ReUtil.get("[^\\x00-\\xff]+",hotRes,0);
            }
            String res = index+"\t"+hot+"\t"+text;
            if(StrUtil.isNotEmpty(keyword)){
                //添加关键词，如：【综艺】【音乐】等
                res = index+"\t"+hot+"\t【"+keyword+"】"+text;
            }
            System.out.println(res);
        }
    }

    /**
     * 匹配中文
     */
    @Test
    void strMatchTest(){
        String str = "晚会 380521";
        String res = ReUtil.get("[^\\x00-\\xff]+", str, 0);
        System.out.println(res);
    }

    /**
     * 新浪微博数据抓取第一步
     *
     * @throws IOException
     */
    private String jsoupWbTid() throws IOException {
        String url = "https://passport.weibo.com/visitor/genvisitor";
        Map<String,Object> params = new HashMap<>();
        params.put("cb","gen_callback");
        String reqRes = HttpUtil.get(url, params, 3000);
        String jsonStr = reqRes.substring(reqRes.indexOf("(") + 1, reqRes.indexOf(")"));
        System.out.println(jsonStr);
        JSONObject res = JSONObject.parseObject(jsonStr);
        String tid = "";
        if(res.getIntValue("retcode")==20000000){
            tid = res.getJSONObject("data").getString("tid");
            System.out.println(tid);
        }
        return tid;
    }
}