package com.jz.baiduHotSearch.job;

import com.jz.baiduHotSearch.service.HotSearchInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 定时任务
 *
 * @author jilt
 * @Date 2024/8/11 20:49
 */
@Slf4j
@Component
public class ScheduleTask {

    @Autowired
    private HotSearchInfoService hotSearchInfoService;

//    @Scheduled(cron = "0 0/2 * * * ?")
    @Scheduled(cron = "0 0 0/4 * * ? ")
    public String grabXlwbHotSearchData() {
        log.info("开始抓取热搜数据-----------------");
        hotSearchInfoService.xlwbHotSearchInsert();
        log.info("抓取热搜数据结束-----------------");
        return "成功";
    }
}
