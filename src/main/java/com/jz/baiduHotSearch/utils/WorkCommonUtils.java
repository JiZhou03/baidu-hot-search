package com.jz.baiduHotSearch.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 通用工具类
 *
 * @author jilt
 * @Date 2023/7/30 10:50
 */
public class WorkCommonUtils {
    public static boolean stringToBoolean(String input){
        if(input.equals("N")){
            return false;
        }
        return true;
    }

    /**
     * 创建文件，如果存在则不创建，可以向文件中填充内容
     *
     * @param filePath
     * @param fileName
     * @param content
     * @throws IOException
     */
    public static void createFile(String filePath,String fileName,String content) throws IOException {
        File dir = new File(filePath);
        // 一、检查放置文件的文件夹路径是否存在，不存在则创建
        if (!dir.exists()) {
            dir.mkdirs();// mkdirs创建多级目录
        }
        File checkFile = new File(filePath + "/" +fileName);
        FileWriter writer = null;
        try {
            // 二、检查目标文件是否存在，不存在则创建
            if (!checkFile.exists()) {
                checkFile.createNewFile();// 创建目标文件
            }
            // 三、向目标文件中写入内容
            writer = new FileWriter(checkFile, false);
            writer.append(content);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != writer)
                writer.close();
        }
    }

    /**
     * 创建文件，如果存在则不创建
     *
     * @param filePath
     * @param fileName
     * @return
     * @throws IOException
     */
    public static String createFile(String filePath,String fileName) throws IOException {
        File dir = new File(filePath);
        // 一、检查放置文件的文件夹路径是否存在，不存在则创建
        if (!dir.exists()) {
            // mkdirs创建多级目录
            dir.mkdirs();
        }
        File checkFile = new File(filePath + "/" +fileName);
        if (!checkFile.exists()) {
            // 创建目标文件
            checkFile.createNewFile();
        }
        return checkFile.getAbsolutePath();
    }
}
