package com.jz.baiduHotSearch.controller;

import com.jz.baiduHotSearch.pojo.HotInfo;
import com.jz.baiduHotSearch.pojo.HotSearchDO;
import com.jz.baiduHotSearch.service.HotSearchInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Api(tags = "控制层")
@RestController
@RequestMapping("hotSearch")
public class HotSearchController {
    private static final Logger LOGGER = LoggerFactory.getLogger(HotSearchController.class);

    @Autowired
    private HotSearchInfoService hotSearchInfoService;

    /**
     * 抓取热搜数据
     *
     * @return
     * @throws IOException
     */
//    @ApiOperation("抓取热搜数据")
//    @GetMapping("/grabHotSearchData")
//    public String grabHotSearchData() throws IOException {
//        LOGGER.info("开始抓取热搜数据-----------------");
//        hotSearchInfoService.recordHotSearchInfo();
//        LOGGER.info("抓取热搜数据结束-----------------");
//        return "success";
//    }

//    /**
//     * 查询信息历史
//     * @param query
//     * @param id
//     * @return
//     */
//    @ApiOperation("查询信息历史")
//    @GetMapping("/findHotInfoHistoryList")
//    public Map<String,Object> findHotInfoHistoryList(@RequestParam("query") String query,
//                                                           @RequestParam("id") String id){
//        LOGGER.info("开始查询信息历史-----------------");
//        return hotSearchInfoService.findHotInfoHistoryList(query,id);
//    }

    @ApiOperation("A01-抓取热搜数据(手动执行定时任务)")
    @GetMapping("/xlwb/grabHotSearchData")
    public String grabXlwbHotSearchData() {
        LOGGER.info("开始抓取热搜数据-----------------");
        hotSearchInfoService.xlwbHotSearchInsert();
        LOGGER.info("抓取热搜数据结束-----------------");
        return "成功";
    }

    /**
     * 查询热搜消息
     * @return
     */
    @ApiOperation("A02-查询热搜消息")
    @GetMapping("/findHotInfoList")
    public List<HotSearchDO> findHotInfoList(@RequestParam(value = "keyWord",required = false) String query){
        LOGGER.info("开始查询热搜消息-----------------");
        return hotSearchInfoService.findHotInfoList(query);
    }

    @ApiOperation("A03-查询热搜消息排名")
    @GetMapping("/findHotInfoSortList")
    public List<HotSearchDO> findHotInfoSortList(@RequestParam(value = "keyWord",required = false) String keyWord,
                                                 @RequestParam(value = "startDate",required = false) String startDate,
                                                 @RequestParam(value = "endDate",required = false) String endDate){
        LOGGER.info("开始查询热搜消息-----------------");
        return hotSearchInfoService.findHotInfoSortList(keyWord,startDate,endDate);
    }


}
