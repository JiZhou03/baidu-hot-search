package com.jz.baiduHotSearch.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 热搜表DO
 * T_HOT_SEARCH
 */
@ApiModel("热搜表DO")
@Data
public class HotSearchDO implements Serializable {
	private static final long serialVersionUID = 1723374516956L;

	@ApiModelProperty("ID")
	private int stId;

	@ApiModelProperty("排名（查询）")
	private int rowSort;

	@ApiModelProperty("排名(批次)")
	private int nmSort;

	@ApiModelProperty("热搜指数")
	private long nmIndex;

	@ApiModelProperty("热搜内容")
	private String stContent;

	@ApiModelProperty("数据来源平台")
	private String stPlatform;

	@ApiModelProperty("创建时间")
	private Date dtCreateDate;
}
